/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Events.hh"

using namespace Theia::Vulkan::Events;
using namespace std;

using Coeus::Severity;

Debug::id_type const Debug::Id = type_index(typeid(Debug));

Debug::Debug(
	decltype(_severity) const sev,
	decltype(_type) const type,
	decltype(_data) data
) :
	_severity(sev),
	_type(type),
	_data(data)
{}

auto Debug::Severity() const -> decltype(_severity) const & {
	return _severity;
}

auto Debug::Type() const -> decltype(_type) const & {
	return _type;
}

auto Debug::Data() const -> decltype(_data) {
	return _data;
}

auto Debug::LogSeverity() const -> Coeus::Severity {
	if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
		return Severity::Error;
	else if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
		return Severity::Warning;
	else if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
		return Severity::Info;
	else if(Severity() >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
		return Severity::Debug;
	else
		return Severity::Error;
}

auto Debug::TypeName() const -> string {
	switch(Type()) {
		case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
			return "General"s;
		case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
			return "Validation"s;
		case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
			return "Performance"s;
		default:
			return "Unknown"s;
	}
}

auto Debug::Message() const -> string {
	// 2020-04-11 AMR TODO: Queue Labels, Command Buffer Labels
	return TypeName() + ": "s + Data()->pMessageIdName + ": "s + Data()->pMessage;
}
