/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_QUEUEINFO_IMPL
#	define THEIA_VULKAN_QUEUEINFO_IMPL

#	include <algorithm>
#	include <functional>

	namespace Theia
	{
		inline namespace Vulkan
		{
			template<typename C>
			auto QueueInfo::Convert(C const &queueInfos) -> std::vector<VkDeviceQueueCreateInfo> {
				auto ret = std::vector<VkDeviceQueueCreateInfo>(queueInfos.size());

				std::transform(
					std::cbegin(queueInfos),
					std::cend(queueInfos),
					std::begin(ret),
					std::bind(&QueueInfo::Info, std::placeholders::_1)
				);

				return ret;
			}
		}
	}

#endif
