/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "PhysicalDevice.hh"
#include <map>
#include "detail/vkvec.hh"

using namespace Theia::Vulkan;
using namespace std;

PhysicalDevice::PhysicalDevice(
	decltype(_instance) const &instance,
	decltype(_handle) && handle
) :
	enable_shared_from_this(),
	_instance(instance),
	_handle(move(handle)),
	_prop(),
	_has_prop(false),
	_features(),
	_has_features(false),
	_mem_prop(),
	_has_mem_prop(false),
	_families()
{}

PhysicalDevice::PhysicalDevice(PhysicalDevice &&) = default;

auto PhysicalDevice::Handle() const -> decltype(_handle) const & {
	return _handle;
}

auto PhysicalDevice::getInstance() const -> decltype(_instance) const & {
	return _instance;
}

auto PhysicalDevice::Properties() -> decltype(_prop) const & {
	if(!_has_prop) {
		vkGetPhysicalDeviceProperties(_handle, &_prop),
		_has_prop = true;
	}

	return _prop;
}

auto PhysicalDevice::Features() -> decltype(_features) const & {
	if(!_has_features) {
		vkGetPhysicalDeviceFeatures(_handle, &_features),
		_has_features = true;
	}

	return _features;
}

auto PhysicalDevice::MemoryProperties() -> decltype(_mem_prop) const & {
	if(!_has_mem_prop) {
		vkGetPhysicalDeviceMemoryProperties(_handle, &_mem_prop),
		_has_mem_prop = true;
	}

	return _mem_prop;
}

auto PhysicalDevice::QueueFamilies() -> decltype(_families) const & {
	if(_families.empty())
		_families = QueueFamily::get(shared_from_this());

	return _families;
}

auto PhysicalDevice::select(
	shared_ptr<Instance> const &instance,
	function<int(shared_ptr<PhysicalDevice> const &)> const &weight
) -> shared_ptr<PhysicalDevice> {
	auto devices = detail::vkvec<VkPhysicalDevice>(instance);
	auto which = multimap<int, shared_ptr<PhysicalDevice>>{};

	for(auto &device : devices) {
		auto phys = make_shared<PhysicalDevice>(move(PhysicalDevice(instance, move(device))));
		which.emplace(weight(phys), phys);
	}

	if(which.rbegin() != which.rend() && which.rbegin()->first > 0)
		return which.rbegin()->second;
	else
		COEUS_THROW(Exception("Failed to find suitable PhysicalDevice"s));
}
