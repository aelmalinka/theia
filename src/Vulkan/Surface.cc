/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Surface.hh"

#ifdef HAVE_GLFW
#	define GLFW_INCLUDE_VULKAN
#	include <GLFW/glfw3.h>
#endif

using namespace Theia::Vulkan;
using namespace std;

namespace Theia { inline namespace Vulkan {
#	ifdef HAVE_GLFW
		template<>
		Surface::Surface(shared_ptr<Instance> const &inst, GLFWwindow *window) :
			_instance(inst),
			_handle(VK_NULL_HANDLE)
		{
			EVK_SUCCESS_OR_THROW(
				glfwCreateWindowSurface(_instance->Handle(), window, nullptr, &_handle),
				"Failed to create Surface"s
			);
		}
#	endif
}}

Surface::~Surface() {
	vkDestroySurfaceKHR(_instance->Handle(), _handle, nullptr);
}

auto Surface::Handle() const -> decltype(_handle) const & {
	return _handle;
}

auto Surface::getInstance() const -> decltype(_instance) const & {
	return _instance;
}
