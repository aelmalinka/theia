/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_INSTANCE_INC
#	define THEIA_VULKAN_INSTANCE_INC

#	include "ApplicationInfo.hh"
#	include "Debug.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			// 2020-04-08 AMR TODO: include debug/validation in instance creation?
			class Instance {
				private:
					using VersionType = decltype(ApplicationInfo::Version);
				private:
					VkInstance _handle;
					ApplicationInfo _info;
					Debug _debug;
				public:
					Instance(std::string && = "Default Application", VersionType const &version = ApplicationInfo::Version);
					Instance(ApplicationInfo &&);
					~Instance();
					Instance(Instance const &) = delete;
					auto Handle() const -> decltype(_handle) const &;
					auto Info() const -> decltype(_info) const &;
			};
		}
	}

#endif
