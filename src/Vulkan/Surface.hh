/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_SURFACE_INC
#	define THEIA_VULKAN_SURFACE_INC

#	include "Instance.hh"

	namespace Theia
	{
		inline namespace Vulkan
		{
			class Surface {
				private:
					std::shared_ptr<Instance> _instance;
					VkSurfaceKHR _handle;
				public:
					template<typename Window>
					Surface(std::shared_ptr<Instance> const &, Window);
					~Surface();
					Surface(Surface const &) = delete;
					auto Handle() const -> decltype(_handle) const &;
					auto getInstance() const -> decltype(_instance) const &;
			};
		}
	}

#endif
