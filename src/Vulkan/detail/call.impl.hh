/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_CALL_IMPL
#	define THEIA_VULKAN_DETAIL_CALL_IMPL

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				call<T>::call(std::string const &name) noexcept :
					_call(reinterpret_cast<T>(
						vkGetInstanceProcAddr(nullptr, name.c_str())
					))
				{}

				template<typename T>
				call<T>::call(std::shared_ptr<Instance> const &inst, std::string const &name) noexcept :
					_call(reinterpret_cast<T>(
						vkGetInstanceProcAddr(inst->Handle(), name.c_str())
					))
				{}

				template<typename T>
				template<typename ...Args>
				auto call<T>::operator () (Args && ...args) const noexcept -> std::invoke_result_t<T, Args...> {
					return _call(std::forward<Args>(args)...);
				}

				template<typename T>
				call<T>::operator bool () const noexcept {
					return _call != nullptr;
				}
			}
		}
	}

#endif
