/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_DETAIL_CALL_INC
#	define THEIA_VULKAN_DETAIL_CALL_INC

#	include "../Instance.hh"
#	include <type_traits>

	namespace Theia
	{
		inline namespace Vulkan
		{
			namespace detail
			{
				template<typename T>
				class call {
					private:
						T _call;
					public:
						explicit call(std::string const &) noexcept;
						call(std::shared_ptr<Instance> const &, std::string const &) noexcept;
						template<typename ...Args>
						auto operator () (Args && ...) const noexcept -> std::invoke_result_t<T, Args...>;
						explicit operator bool () const noexcept;
				};
			}
		}
	}

#	include "call.impl.hh"

#endif
