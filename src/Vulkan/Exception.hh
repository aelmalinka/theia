/*	Copyright 2018 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined THEIA_VULKAN_EXCEPTION_INC
#	define THEIA_VULKAN_EXCEPTION_INC

#	include "../Exception.hh"
#	include <cstdint>
#	include <vulkan/vulkan.h>

	namespace Theia
	{
		inline namespace Vulkan
		{
			COEUS_EXCEPTION(NotFound, "Required Not Found", ::Theia::Exception);
			COEUS_ERROR_INFO(Name, std::string);
			COEUS_ERROR_INFO(Result, VkResult);
		}
	}

#	define EVK_SUCCESS_OR_THROW(f, x) { \
		VkResult ok = (f); \
		if(ok != VK_SUCCESS) \
			::boost::throw_exception(::boost::enable_error_info(::Theia::Exception(x)) << \
			::boost::throw_function(BOOST_CURRENT_FUNCTION) << \
			::boost::throw_file(__FILE__) << \
			::boost::throw_line((int)__LINE__) << \
			::Theia::Vulkan::Result(ok)); \
	}

#endif
