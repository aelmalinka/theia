/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Exception.hh"

using namespace std;
using namespace Coeus;

namespace Theia {
	Log::Source Log("Theia"s);
}
