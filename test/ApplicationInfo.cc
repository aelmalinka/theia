/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Vulkan/ApplicationInfo.hh"
#include <iostream>

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

namespace {
	TEST(ApplicationInfoTests, Create) {
		auto info = ApplicationInfo("TEST APPLICATION", 1);

		EXPECT_EQ(info.Handle().sType, VK_STRUCTURE_TYPE_APPLICATION_INFO);
		EXPECT_EQ(info.Extensions().size(), 0u);
		EXPECT_EQ(info.Layers().size(), 0u);
	}

	TEST(ApplicationInfoTests, AddDebugExt) {
		auto info = ApplicationInfo("TEST APPLICATION", 1).Add(Extension(VK_EXT_DEBUG_UTILS_EXTENSION_NAME));

		EXPECT_EQ(info.Handle().sType, VK_STRUCTURE_TYPE_APPLICATION_INFO);
		EXPECT_EQ(info.Extensions().size(), 1u);
		EXPECT_EQ(info.Layers().size(), 0u);
	}

	TEST(ApplicationInfoTests, AddValidationLayer) {
		auto info = ApplicationInfo("TEST APPLICATION", 1).Add(Layer("VK_LAYER_LUNARG_standard_validation"));

		EXPECT_EQ(info.Handle().sType, VK_STRUCTURE_TYPE_APPLICATION_INFO);
		EXPECT_EQ(info.Extensions().size(), 0u);
		EXPECT_EQ(info.Layers().size(), 1u);
	}

	TEST(ApplicationInfoTests, AddExtensionNotFound) {
		auto info = ApplicationInfo("TEST APPLICATION", 1).Add(Extension("asdf"));

		EXPECT_EQ(info.Handle().sType, VK_STRUCTURE_TYPE_APPLICATION_INFO);
		EXPECT_EQ(info.Extensions().size(), 0u);
		EXPECT_EQ(info.Layers().size(), 0u);
	}

	TEST(ApplicationInfoTests, AddLayerNotFound) {
		auto info = ApplicationInfo("TEST APPLICATION", 1).Add(Layer("asdf"));

		EXPECT_EQ(info.Handle().sType, VK_STRUCTURE_TYPE_APPLICATION_INFO);
		EXPECT_EQ(info.Extensions().size(), 0u);
		EXPECT_EQ(info.Layers().size(), 0u);
	}

	TEST(ApplicationInfoTests, AddExtensionNotFoundThrow) {
		EXPECT_THROW(ApplicationInfo("TEST APPLICATION", 1).Add(Extension("asdf", true)), NotFound);
	}

	TEST(ApplicationInfoTests, AddLayerNotFoundThrow) {
		EXPECT_THROW(ApplicationInfo("TEST APPLICATION", 1).Add(Layer("asdf", true)), NotFound);
	}
}
