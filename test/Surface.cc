/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include <gtest/gtest.h>
#include "../src/Vulkan/Surface.hh"

#ifdef HAVE_GLFW
#	define GLFW_INCLUDE_VULKAN
#	include <GLFW/glfw3.h>
#endif

using namespace std;
using namespace testing;
using namespace Theia::Vulkan;

using Theia::Exception;

auto const WIDTH = 800;
auto const HEIGHT = 600;

namespace {
	auto make_instance() {
		return make_shared<Instance>("Theia Surface Tests"s, ApplicationInfo::Version);
	}

#	ifdef HAVE_GLFW
		class SurfaceGlfwTests :
			public Test
		{
			protected:
				decltype(glfwCreateWindow(WIDTH, HEIGHT, "", nullptr, nullptr)) window;
				void SetUp() override {
					ASSERT_TRUE(glfwInit());

					glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
					glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);		// 2020-04-06 AMR TODO: do we want this? does it matter?

					window = glfwCreateWindow(WIDTH, HEIGHT, "Theia Surface Tests", nullptr, nullptr);
				}
				void TearDown() override {
					glfwDestroyWindow(window);
					glfwTerminate();
				}
		};

		TEST_F(SurfaceGlfwTests, Create) {
			auto inst = make_instance();
			auto surf = make_shared<Surface>(inst, window);

			EXPECT_FALSE(inst->Handle() == VK_NULL_HANDLE);
			EXPECT_FALSE(surf->Handle() == VK_NULL_HANDLE);
			EXPECT_EQ(surf->getInstance(), inst);
		}
#	endif
}
